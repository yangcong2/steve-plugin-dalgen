package com.huixian.plugin.dalgen.bean;

import com.huixian.plugin.dalgen.util.StringUtils;

/**
 * 字段信息
 */
public class TableField {
    private boolean keyFlag;
    private String name;
    private String type;
    private String propertyName;
    private String propertyType;
    private String propertyClassName;
    private String comment;
    private String extra;
    private Boolean isEnum;

    public boolean getIsEnum() {
        return isEnum == null ? false : isEnum;
    }

    public void setEnum(boolean anEnum) {
        isEnum = anEnum;
    }

    public boolean isKeyFlag() {
        return keyFlag;
    }

    public void setKeyFlag(boolean keyFlag) {
        this.keyFlag = keyFlag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyClassName = StringUtils.toFirstUpperCase(propertyName);
        this.propertyName = propertyName;
    }

    public String getPropertyClassName() {
        return propertyClassName;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isConvert() {
        return !name.equals(propertyName);
    }

    public String getCapitalName() {
        return propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
