package com.huixian.plugin.dalgen.rule;

/**
 * 数据库类型定义
 */
public enum DbType {

    MYSQL("mysql"), ORACLE("oracle");

    private String value;

    DbType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
