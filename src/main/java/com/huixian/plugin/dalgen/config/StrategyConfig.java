package com.huixian.plugin.dalgen.config;

import com.huixian.plugin.dalgen.rule.IdStrategy;
import com.huixian.plugin.dalgen.rule.NamingStrategy;
import com.huixian.plugin.dalgen.util.StringUtils;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * 策略配置项
 */
public class StrategyConfig {

	/**
	 * 数据库表映射到实体的命名策略
	 */
	@Parameter(defaultValue = "underline_to_camel")
	private NamingStrategy naming;

	private NamingStrategy fieldNaming;

	/**
	 * 表前缀
	 */
	@Parameter
	private String tablePrefix;

	/**
	 * Entity 中的ID生成类型
	 */
	@Parameter(defaultValue = "ID_WORKER")
	private IdStrategy idGenType;

	/**
	 * 自定义继承的Entity类全称，带包名
	 */
	@Parameter(defaultValue = ConstVal.SUPERD_ENTITY_CLASS)
	private String superEntityClass;

	/**
	 * 自定义继承的Mapper类全称，带包名
	 */
	@Parameter(defaultValue = ConstVal.SUPERD_MAPPER_CLASS)
	private String superMapperClass;

	/**
	 * 自定义继承的Service类全称，带包名
	 */
	@Parameter(defaultValue = ConstVal.SUPERD_SERVICE_CLASS)
	private String superServiceClass;

	/**
	 * 自定义继承的ServiceImpl类全称，带包名
	 */
	@Parameter(defaultValue = ConstVal.SUPERD_SERVICEIMPL_CLASS)
	private String superServiceImplClass;

	/**
	 * 自定义继承的Controller类全称，带包名
	 */
	@Parameter
	private String superControllerClass;

	/*
	 * 需要包含的表名（与exclude二选一配置）
	 */
	@Parameter
	private String[] include = null;

	/**
	 * 需要排除的表名
	 */
	@Parameter
	private String[] exclude = null;

	public void setSuperEntityClass(String superEntityClass) {
		this.superEntityClass = superEntityClass;
	}

	public NamingStrategy getNaming() {
		if(naming == null){
			return NamingStrategy.underline_to_camel;
		}
		return naming;
	}

	public NamingStrategy getFieldNaming() {
		if (fieldNaming == null)
			return NamingStrategy.underline_to_camel;
		return fieldNaming;
	}


	public String getTablePrefix() {
		return tablePrefix;
	}

	public IdStrategy getIdGenType() {
		return idGenType;
	}

	public String[] getInclude() {
		return include;
	}

	public String[] getExclude() {
		return exclude;
	}

	public String getSuperServiceClass() {
		return StringUtils.isBlank(superServiceClass) ? superServiceClass : ConstVal.SUPERD_SERVICE_CLASS;
	}

	public String getSuperEntityClass() {
		return StringUtils.isBlank(superEntityClass) ? superEntityClass : ConstVal.SUPERD_ENTITY_CLASS;
	}

	public String getSuperMapperClass() {
		return StringUtils.isBlank(superMapperClass) ? superMapperClass : ConstVal.SUPERD_MAPPER_CLASS;
	}

	public String getSuperServiceImplClass() {
		return StringUtils.isBlank(superServiceImplClass) ? superServiceImplClass : ConstVal.SUPERD_SERVICEIMPL_CLASS;
	}

	public String getSuperControllerClass() {
		return StringUtils.isBlank(superControllerClass) ? superControllerClass : ConstVal.SUPERD_CONTROLLER_CLASS;
	}

}
