package com.huixian.plugin.dalgen.config;

import com.huixian.plugin.dalgen.util.StringUtils;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * 跟包相关的配置项
 */
public class PackageConfig {

    /**
     * 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
     */
    @Parameter(defaultValue = "com.huixian")
    private String parent;

    /**
     * Entity包名
     */
    @Parameter(defaultValue = "entity")
    private String entity;

    /**
     * Entity包名
     */
    @Parameter(defaultValue = "enums")
    private String enums;

    /**
     * Service包名
     */
    @Parameter(defaultValue = "service")
    private String service;

    /**
     * Service Impl包名
     */
    @Parameter(defaultValue = "service.impl")
    private String serviceImpl;
    /**
     * Mapper包名
     */
    @Parameter(defaultValue = "mapper")
    private String mapper;

    /**
     * Mapper XML包名
     */
    @Parameter(defaultValue = "mapper.xml")
    private String xml;

    private String modelName;
    
    /**
     * Controller包名
     */
    @Parameter(defaultValue = "controller")
    private String controller;

    public String getParent() {
        if(StringUtils.isBlank(parent)){
            return "com.huixian";
        }
        return parent;
    }

    public String getEntity() {
        if(StringUtils.isBlank(entity))
            return "dataobject";
        return entity;
    }
    public String getEnums() {
        if(StringUtils.isBlank(enums))
            return "enums";
        return enums;
    }

    public String getService() {
        if(StringUtils.isBlank(service))
            return "repository";
        return service;
    }

    public String getServiceImpl() {
        if(StringUtils.isBlank(serviceImpl))
            return "repository";
        return serviceImpl;
    }

    public String getMapper() {
        if(StringUtils.isBlank(mapper))
            return "mapper";
        return mapper;
    }

    public String getXml() {
        if(StringUtils.isBlank(xml))
            return "mapper";
        return xml;
    }
    
    public String getController() {
    	if(StringUtils.isBlank(controller))
    	    return "controller";
    	return controller;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
