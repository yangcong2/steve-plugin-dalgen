package com.huixian.plugin.dalgen;

import com.huixian.plugin.dalgen.config.PackageConfig;
import com.huixian.plugin.dalgen.config.DataSourceConfig;
import com.huixian.plugin.dalgen.config.StrategyConfig;
import com.huixian.plugin.dalgen.config.TemplateConfig;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;

/**
 * 插件基类，用于属性配置 设计成抽象类主要是用于后期可扩展，共享参数配置。
 *
 */
public abstract class AbstractGenerateMojo extends AbstractMojo {

	/**
	 * 数据源配置
	 */
	@Parameter(required = true)
	private DataSourceConfig dataSource;

	/**
	 * 数据库表配置
	 */
	@Parameter
	private StrategyConfig strategy;

	/**
	 * 包 相关配置
	 */
	@Parameter
	private PackageConfig packageInfo;

	/**
	 * 模板 相关配置
	 */
	@Parameter
	private TemplateConfig template;

	/**
	 * 生成文件的输出目录
	 */
	@Parameter(defaultValue = "${project.basedir}")
	private String baseDir;

	/**
	 * 项目名
	 */
	@Parameter(defaultValue = "${artifactId}")
	private String projectName;

	/**
	 * 是否覆盖已有文件
	 */
	@Parameter(defaultValue = "true")
	private boolean fileOverride;

	/**
	 * 是否打开输出目录
	 */
	@Parameter(defaultValue = "false")
	private boolean open;

	/**
	 * 是否在xml中添加二级缓存配置
	 */
	@Parameter(defaultValue = "false")
	private boolean enableCache;

	/**
	 * 开发人员
	 */
	@Parameter(defaultValue = "author")
	private String author;

	/**
	 * 开启 ActiveRecord 模式
	 */
	@Parameter(defaultValue = "false")
	private boolean activeRecord;

	protected ConfigBuilder config;

	/**
	 * 日志工具
	 */
	protected Log log = getLog();

	/**
	 * 初始化配置
	 */
	protected void initConfig() {
		if (null == config) {
			if(packageInfo == null){
				packageInfo = new PackageConfig();
			}
			packageInfo.setModelName(getProjectNameToUnderLine());
			if(strategy == null){
				strategy = new StrategyConfig();
			}
			config = new ConfigBuilder(packageInfo, dataSource, strategy, template, getBaseDir());
		}
	}

	public String getBaseDir() {
		return baseDir + File.separator + "src" + File.separator + "main";
	}

	public String getAuthor() {
		return author;
	}

	public boolean isFileOverride() {
		return fileOverride;
	}

	public boolean isOpen() {
		return open;
	}

	public boolean isEnableCache() {
		return enableCache;
	}

	public boolean isActiveRecord() {
		return activeRecord;
	}

	public String getProjectName() {
		return projectName;
	}

	public String getProjectNameToUnderLine() {
		// 兼容去除huixian开头的项目
		if(projectName.startsWith("huixian-")) {
			projectName = projectName.replace("huixian-", "");
		}
		return projectName.replaceAll("-", ".");
	}
}
