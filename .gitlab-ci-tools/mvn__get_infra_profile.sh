#!/bin/bash

# 获取parent和组件包项目编译环境
# 1. 分支为master，环境为release
# 2. 其他分支，环境为test
# 3. 默认环境为test

_PROFILE=test

# 检测分支，如果为master则改为release
if [ "${CI_COMMIT_REF_NAME}" == "master" ] ; then
	_PROFILE=release
fi

echo "$_PROFILE"
